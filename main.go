package main

import "fmt"

func Solution(array []int, cycle int) []int {

	newArray := make([]int, len(array))

	leng := len(array)

	var trueCycle int

	if cycle >= leng {
		trueCycle = cycle - (cycle/leng)*leng
	} else {
		trueCycle = cycle
	}

	if trueCycle != 0 {

		for i := 0; i < trueCycle; i++ {
			newArray[((trueCycle - 1) - i)] = array[(leng-1)-i]

			if i == (trueCycle - 1) {
				for j := i; j < leng-1; j++ {

					newArray[j+1] = array[j-i]

				}
			}
		}
		return newArray
	}
	return array
}

func main() {

	A := []int{3, 8, 9, 7, 6}

	B := Solution(A, 3)
	fmt.Println("res = ", B)
	B = Solution(B, 3)
	fmt.Println("res = ", B)
	B = Solution(B, 3)
	fmt.Println("res = ", B)

	C := []int{1, 2, 3, 4}

	D := Solution(C, 4)
	fmt.Println("D = ", D)

	E := []int{-1000, -10, -1, -2, -0, -3, 0, 20, 4, 5, 6}

	F := Solution(E, 0)
	fmt.Println("F = ", F)

	F = Solution(E, 100)
	fmt.Println("F = ", F)
}
